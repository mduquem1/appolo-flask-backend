from app.views.views import MyStore
from flask_restful import Api

from ..views import HealthyCheck, SignUp, Login, Stores, MyStore


def create_api(app):
    api = Api(app)
    api.add_resource(HealthyCheck, '/')
    api.add_resource(SignUp, '/api/auth/signup')
    api.add_resource(Login, '/api/auth/login')
    api.add_resource(Stores, '/api/stores')
    api.add_resource(MyStore, '/api/me')
    
    return api