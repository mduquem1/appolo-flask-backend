import os
from flask import Flask
# from python_decouple import config

def create_app(config_dict = {}):
    app = Flask(__name__)
   #  app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://{}:{}@{}:{}/{}".format(
   #     os.environ.get("DB_USERNAME"),
   #     os.environ.get("DB_PASSWORD"),
   #     os.environ.get("DB_HOST"),
   #     os.environ.get("DB_PORT"),
   #     os.environ.get("DB_NAME"),
   #  )
    app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://appolo:password@localhost:5432/appolodb"
    app.config["JWT_SECRET_KEY"] = "appolo-super-secret-password-needed-for-jwt"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["isolation_level"] = "READ COMMITTED"
    app.config["PROPAGATE_EXCEPTIONS"] = True
    app.config["TESTING"] = config_dict.get("TESTING", False)

    return app