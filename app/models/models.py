from flask_sqlalchemy import SQLAlchemy
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

db = SQLAlchemy()

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    password = db.Column(db.String(50))
    email = db.Column(db.String(50), unique=True)
    is_admin = db.Column(db.Boolean, default=False)
    is_daycare = db.Column(db.Boolean, default=False)
    is_hotel = db.Column(db.Boolean, default=False)
    is_parent = db.Column(db.Boolean, default=True)
    stores = db.relationship("Store", cascade="all, delete, delete-orphan")

class Store(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), unique=True)
    description = db.Column(db.String(500))
    registered_since = db.Column(db.String(128))
    email = db.Column(db.String(128))
    address = db.Column(db.String(128))
    city = db.Column(db.String(128))
    department = db.Column(db.String(128))
    phone = db.Column(db.String(128))
    photo_url = db.Column(db.String(127))
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    user = db.relationship("User", back_populates="stores")

class UserSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = User
        include_relationships = True
        load_instance = True

class StoreSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Store
        include_relationships = True
        load_instance = True