import datetime
from flask_restful import Resource, request
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity
from ..models import db, User, Store, StoreSchema
from sqlalchemy import exc

store_schema = StoreSchema()

class HealthyCheck(Resource):
    def get(self):
        return {'message': 'Healthy', 'status': 200}, 200

class SignUp(Resource):
    def post(self):
        if request.json["password1"] != request.json["password2"]:
            return {"message": "password 1 y password 2 no concuerdan", "isError": True}
        try:
            new_user = User(
                password=request.json["password1"],
                email=request.json["email"],
            )
            db.session.add(new_user)
            db.session.commit()
            token = create_access_token(identity=new_user.id)

            return {"message": "usuario creado con exito", "isError": False, "token": token}
        except exc.SQLAlchemyError:
            db.session.rollback()
            return {"message": "Username o password invalidos", "isError": True}

class Login(Resource):
    def post(self):
        POST_EMAIL = str(request.json["email"])
        POST_PASSWORD = str(request.json["password"])
        query = db.session.query(User).filter(
            User.email.in_([POST_EMAIL]), User.password.in_([POST_PASSWORD])
        )
        result = query.first()
        if result:
            token = create_access_token(identity=result.id)
            return {"token": token, "status": 200}, 200
        else:
            return "Invalid username o password", 401


class Stores(Resource):
    @jwt_required()
    def post(self):
        
        userId = get_jwt_identity()
        user = User.query.get_or_404(userId)

        STORE_NAME = str(request.json["name"])
        STORE_DESCRIPTION = str(request.json["description"])
        STORE_REGISTERED = str(datetime.datetime.now())
        STORE_PHOTOS = "https://www.patasarriba.cl/wp-content/uploads/2018/05/patasarribanota-nota6.jpg"
        STORE_EMAIL = str(user.email)
        STORE_ADDRESS = str(request.json["address"])
        STORE_CITY = str(request.json["city"])
        STORE_DEPARTMENT = str(request.json["department"])
        STORE_PHONE = str(request.json["phone"])
        try:
            new_store = Store(
                name=STORE_NAME,
                description=STORE_DESCRIPTION,
                registered_since=STORE_REGISTERED,
                email=STORE_EMAIL,
                address=STORE_ADDRESS,
                city=STORE_CITY,
                department=STORE_DEPARTMENT,
                phone=STORE_PHONE,
                photo_url=STORE_PHOTOS,
                user_id=userId,
                user=user  
            )
            db.session.add(new_store)
            db.session.commit()
            return store_schema.dump(new_store)
            
        except exc.IntegrityError:
            return (
                "A store with that name already exists, please try again",
                400,
            )
        except Exception as e:
            return (
                "Unidentified error, please try again. Error="
                + str(e),
                400,
            )
    @jwt_required()
    def get(self):
        stores = Store.query.all()
        return [store_schema.dump(store) for store in stores]
       
class MyStore(Resource):
    @jwt_required()
    def get(self):
        userId = get_jwt_identity()
        my_stores = Store.query.filter_by(user_id=userId)
        return [store_schema.dump(store) for store in my_stores]
       
