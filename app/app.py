from flask_jwt_extended import JWTManager
from .models import db
from . import create_app
from .routes import create_api

app = create_app()
app_context = app.app_context()
app_context.push()

db.init_app(app)
db.create_all()

create_api(app)

jwt = JWTManager(app)

if __name__ == "__main__":
    app.run()